# Assets manager #

Manager with [Latte](https://latte.nette.org/) macro that helps to properly load CSS/JS assets.

Useful for assets [cache busting](https://www.keycdn.com/support/what-is-cache-busting) in the production environment.
In the development environment for work with [Browsersync](https://browsersync.io/), the original path of the asset is rendered.

## Requirements ##

- [PHP](https://php.net/) >= 8.3
- [Nette](https://nette.org/) >= 3.2
- [Latte](https://latte.nette.org/) >= 3.0

## Installation ##

The best way to install [**interitty/assets-manager**](https://gitlab.com/interitty/assets-manager) is using [Composer](https://getcomposer.org/):

```bash
composer require interitty/assets-manager
```

Then register the extension in the [Nette config](https://doc.nette.org/en/bootstrap#toc-configuration-files) file:

```neon
# app/config/config.neon
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension
```

## Usage ##

Macro can be used in any [Latte](https://latte.nette.org/) template:

```latte
{* app/presenters/templates/@layout.latte *}
<link rel="stylesheet" href="{asset css/asset.css}">
<script src="{asset js/asset.js}"></script>
```

It prepends the path with ```$assetBasePath``` and loads revision from the [revision manifest](#revision-manifest):

```html
<link rel="stylesheet" href="/assets/css/asset.aHR0cHM6Ly9nb28uZ2wvS1ZRNHpT.css}">
<script src="/assets/js/asset.aHR0cHM6Ly9nb28uZ2wveFZqZHM0.js"></script>
```

## Revision manifest ##

**The revision manifest is a JSON file that contains the revision path of the asset.**

It can be generated by various asset processors such as gulp or other similar tools.

Revision manifest is searched in the ```$manifestPath```.

**The path to revision manifest can be set directly:**
```neon
# app/config/config.neon
assetsManager:
    assetBasePath: /assets/
    manifestPath: %wwwDir%/assets/manifest.json
```

Revision manifest may contain the asset path.

### Asset version in the file name ###

With this method, the files have a **different name at each change**.

Example revision manifest:
```json
{
    "css/asset.css": "css/asset.aHR0cHM6Ly9nb28uZ2wvS1ZRNHpT.css",
    "js/asset.js": "js/asset.aHR0cHM6Ly9nb28uZ2wveFZqZHM0.js"
}
```

With the example manifest, the code `{asset "js/asset.js"}` generates: `/assets/js/asset.aHR0cHM6Ly9nb28uZ2wveFZqZHM0.js` path.

## Configuration ##

The default configuration, that can be changed to suit your needs:

```neon
# app/config/config.neon
assetsManager:
    assetBasePath: ''
    manifestPath: ''
    productionMode: %productionMode%
    wwwDir: %wwwDir%
```
