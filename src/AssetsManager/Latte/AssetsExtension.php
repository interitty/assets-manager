<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Latte;

use Interitty\AssetsManager\AssetsManager;
use Latte\Extension;

class AssetsExtension extends Extension
{
    /** All available latte provider name constants */
    public const string LATTE_MACRO_NAME = 'asset';
    public const string LATTE_PROVIDER_ASSETS_MANAGER = 'assetsManager';

    /** @var AssetsManager */
    protected AssetsManager $assetsManager;

    /**
     * Constructor
     *
     * @param AssetsManager $assetsManager
     * @return void
     */
    public function __construct(AssetsManager $assetsManager)
    {
        $this->setAssetsManager($assetsManager);
    }

    /**
     * @inheritDocs
     */
    public function getProviders(): array
    {
        return [
            self::LATTE_PROVIDER_ASSETS_MANAGER => $this->getAssetsManager(),
        ];
    }

    /**
     * @inheritDocs
     */
    public function getTags(): array
    {
        return [
            self::LATTE_MACRO_NAME => [AssetsNode::class, 'create'],
        ];
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * AssetsManager getter
     *
     * @return AssetsManager
     */
    protected function getAssetsManager(): AssetsManager
    {
        return $this->assetsManager;
    }

    /**
     * AssetsManager setter
     *
     * @param AssetsManager $assetsManager
     * @return static Provides fluent interface
     */
    protected function setAssetsManager(AssetsManager $assetsManager): static
    {
        $this->assetsManager = $assetsManager;
        return $this;
    }

    // </editor-fold>
}
