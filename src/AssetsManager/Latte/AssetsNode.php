<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Latte;

use Generator;
use Latte\Compiler\Nodes\Php\ExpressionNode;
use Latte\Compiler\Nodes\StatementNode;
use Latte\Compiler\PrintContext;
use Latte\Compiler\Tag;

/**
 * {assets assetName}
 */
class AssetsNode extends StatementNode
{
    /** @var ExpressionNode */
    public ExpressionNode $assetNameNode;

    /**
     * Assets node factory
     *
     * @param Tag $tag
     * @return self
     */
    public static function create(Tag $tag): self
    {
        $node = new self();
        $tag->expectArguments();
        $node->assetNameNode = $tag->parser->parseUnquotedStringOrExpression();
        return $node;
    }

    /**
     * {assets 'folder/file.css'}
     *
     * @param PrintContext $context
     * @return string
     */
    public function print(PrintContext $context): string
    {
        $code = 'echo $this->global->' . AssetsExtension::LATTE_PROVIDER_ASSETS_MANAGER . '->renderAsset(%node);';
        $result = $context->format($code, $this->assetNameNode);
        return $result;
    }

    /**
     * Nodes iterator getter
     *
     * @phpstan-return Generator<ExpressionNode>
     */
    public function &getIterator(): Generator
    {
        yield $this->assetNameNode;
    }
}
