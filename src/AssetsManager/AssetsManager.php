<?php

declare(strict_types=1);

namespace Interitty\AssetsManager;

use Interitty\AssetsManager\Assets\Asset;
use Interitty\AssetsManager\Assets\AssetsCollection;
use Interitty\AssetsManager\Assets\AssetsLoaderInterface;
use Interitty\Exceptions\Exceptions;
use Interitty\Utils\Html;
use Interitty\Utils\Validators;
use LogicException;

use function assert;
use function file_exists;
use function ltrim;
use function rtrim;
use function sprintf;

class AssetsManager
{
    /** @var string */
    protected string $assetsBasePath = '';

    /** @var AssetsCollection */
    protected AssetsCollection $assetsCollection;

    /** @var AssetsLoaderInterface */
    protected AssetsLoaderInterface $assetsLoader;

    /** @var bool */
    protected bool $productionMode = true;

    /** @var string */
    protected string $wwwDir = '';

    /**
     * Asset path getter
     *
     * @param string $assetName
     * @return Html
     */
    public function renderAsset(string $assetName): Html
    {
        $asset = $this->getAsset($assetName);
        $assetPath = $this->getAssetPath($asset);
        $assetControl = Html::el()->addText($assetPath);
        return $assetControl;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Asset getter
     *
     * @param string $assetName
     * @return Asset
     */
    public function getAsset(string $assetName): Asset
    {
        $asset = $this->getAssetsCollection()->getAsset($assetName);
        return $asset;
    }

    /**
     * AssetsBasePath getter
     *
     * @return string
     */
    public function getAssetsBasePath(): string
    {
        return $this->assetsBasePath;
    }

    /**
     * AssetsBasePath setter
     *
     * @param string $assetsBasePath
     * @return static Provides fluent interface
     */
    public function setAssetsBasePath($assetsBasePath): static
    {
        assert(Validators::check($assetsBasePath, 'string', 'AssetsBasePath'));
        $this->assetsBasePath = rtrim($assetsBasePath, '/');
        return $this;
    }

    /**
     * AssetsCollection getter
     *
     * @return AssetsCollection
     */
    public function getAssetsCollection(): AssetsCollection
    {
        if (isset($this->assetsCollection) === false) {
            $assetsCollection = $this->getAssetsLoader()->loadAssets();
            $this->setAssetsCollection($assetsCollection);
        }
        return $this->assetsCollection;
    }

    /**
     * AssetsCollection setter
     *
     * @param AssetsCollection $assetsCollection
     * @return static Provides fluent interface
     */
    public function setAssetsCollection(AssetsCollection $assetsCollection): static
    {
        assert(Validators::check(isset($this->assetsCollection), 'uninitialized', 'assetsCollection before set'));
        $this->assetsCollection = $assetsCollection;
        return $this;
    }

    /**
     * AssetsLoader getter
     *
     * @return AssetsLoaderInterface
     */
    public function getAssetsLoader(): AssetsLoaderInterface
    {
        assert(Validators::check(isset($this->assetsLoader), 'initialized', 'assetsLoader before get'));
        return $this->assetsLoader;
    }

    /**
     * AssetsLoader setter
     *
     * @param AssetsLoaderInterface $assetsLoader
     * @return static Provides fluent interface
     */
    public function setAssetsLoader(AssetsLoaderInterface $assetsLoader): static
    {
        assert(Validators::check(isset($this->assetsLoader), 'uninitialized', 'assetsLoader before set'));
        $this->assetsLoader = $assetsLoader;
        return $this;
    }

    /**
     * Asset path getter
     *
     * @param Asset $asset
     * @return string
     */
    public function getAssetPath(Asset $asset): string
    {
        $assetsBasePath = $this->getAssetsBasePath();
        $productionMode = $this->isProductionMode();
        $wwwDir = $this->getWwwDir();
        if ($productionMode === true) {
            $assetPath = $asset->getProductionPath();
        } else {
            $assetPath = $asset->getOriginalPath();
        }
        $relativePath = sprintf('%s/%s', $assetsBasePath, $assetPath);
        $absolutePath = sprintf('%s/%s', $wwwDir, ltrim($relativePath, '/'));
        if (file_exists($absolutePath) !== true) {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('Asset ":asset" not found')
                    ->addData('asset', $absolutePath);
        }
        return $relativePath;
    }

    /**
     * ProductionMode checker
     *
     * @return bool
     */
    public function isProductionMode(): bool
    {
        return $this->productionMode;
    }

    /**
     * ProductionMode setter
     *
     * @param bool $productionMode
     * @return static Provides fluent interface
     */
    public function setProductionMode(bool $productionMode): static
    {
        $this->productionMode = $productionMode;
        return $this;
    }

    /**
     * WwwDir getter
     *
     * @return string
     */
    public function getWwwDir(): string
    {
        return $this->wwwDir;
    }

    /**
     * WwwDir setter
     *
     * @param string $wwwDir
     * @return static Provides fluent interface
     */
    public function setWwwDir($wwwDir): static
    {
        assert(Validators::check($wwwDir, 'string', 'WwwDir'));
        $this->wwwDir = $wwwDir;
        return $this;
    }

    // </editor-fold>
}
