<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

interface AssetFactoryInterface
{
    /**
     * Asset factory
     *
     * @param string $originalPath
     * @param string $productionPath
     * @return Asset
     */
    public function create(string $originalPath, string $productionPath): Asset;
}
