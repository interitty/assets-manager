<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

use Interitty\Exceptions\Exceptions;
use LogicException;

use function array_key_exists;

class AssetsCollection
{
    /** @var Asset[] */
    protected array $assets = [];

    /**
     * Asset adder
     *
     * @param Asset $asset
     * @return static Provides fluent interface
     */
    public function addAsset(Asset $asset)
    {
        $assetOriginalPath = $asset->getOriginalPath();
        if ($this->isAssetExists($assetOriginalPath) !== false) {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('Asset ":asset" is already registered.')
                    ->addData('asset', $assetOriginalPath);
        }
        $this->assets[$assetOriginalPath] = $asset;
        return $this;
    }

    /**
     * Asset getter
     *
     * @param string $assetOriginalPath
     * @return Asset
     */
    public function getAsset(string $assetOriginalPath): Asset
    {
        if ($this->isAssetExists($assetOriginalPath) !== true) {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('Asset ":asset" is not registered.')
                    ->addData('asset', $assetOriginalPath);
        }
        return $this->assets[$assetOriginalPath];
    }

    /**
     * Asset exists checker
     *
     * @param string $assetOriginalPath
     * @return bool
     */
    public function isAssetExists(string $assetOriginalPath): bool
    {
        $assetExists = array_key_exists($assetOriginalPath, $this->assets);
        return $assetExists;
    }
}
