<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

interface AssetsLoaderInterface
{
    /**
     * Assets loader
     *
     * @return AssetsCollection
     */
    public function loadAssets(): AssetsCollection;
}
