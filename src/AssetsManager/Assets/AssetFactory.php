<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

class AssetFactory implements AssetFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(string $originalPath, string $productionPath): Asset
    {
        $asset = new Asset($originalPath, $productionPath);
        return $asset;
    }
}
