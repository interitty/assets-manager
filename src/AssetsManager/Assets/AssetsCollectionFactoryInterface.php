<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

interface AssetsCollectionFactoryInterface
{
    /**
     * AssetsCollection factory
     *
     * @return AssetsCollection
     */
    public function create(): AssetsCollection;
}
