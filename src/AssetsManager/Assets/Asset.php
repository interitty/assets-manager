<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

use Interitty\Utils\Validators;

use function assert;

class Asset
{
    /** @var string */
    protected string $originalPath;

    /** @var string */
    protected string $productionPath;

    /**
     * Constructor
     *
     * @param string $originalPath
     * @param string $productionPath
     * @return void
     */
    public function __construct(string $originalPath, string $productionPath)
    {
        $this->setOriginalPath($originalPath);
        $this->setProductionPath($productionPath);
    }
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">

    /**
     * OriginalPath getter
     *
     * @return string
     */
    public function getOriginalPath(): string
    {
        return $this->originalPath;
    }

    /**
     * OriginalPath setter
     *
     * @param string $originalPath
     * @return static Provides fluent interface
     */
    public function setOriginalPath($originalPath): static
    {
        assert(Validators::check(isset($this->originalPath), 'uninitialized', 'originalPath before set'));
        assert(Validators::check($originalPath, 'string', 'OriginalPath'));
        $this->originalPath = $originalPath;
        return $this;
    }

    /**
     * ProductionPath getter
     *
     * @return string
     */
    public function getProductionPath(): string
    {
        return $this->productionPath;
    }

    /**
     * ProductionPath setter
     *
     * @param string $productionPath
     * @return static Provides fluent interface
     */
    public function setProductionPath($productionPath): static
    {
        assert(Validators::check(isset($this->productionPath), 'uninitialized', 'productionPath before set'));
        assert(Validators::check($productionPath, 'string', 'ProductionPath'));
        $this->productionPath = $productionPath;
        return $this;
    }
    // </editor-fold>
}
