<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

class AssetsCollectionFactory implements AssetsCollectionFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(): AssetsCollection
    {
        $assetsCollection = new AssetsCollection();
        return $assetsCollection;
    }
}
