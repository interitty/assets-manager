<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Manifest;

use Interitty\AssetsManager\Assets\AssetFactoryInterface;
use Interitty\AssetsManager\Assets\AssetsCollection;
use Interitty\AssetsManager\Assets\AssetsCollectionFactoryInterface;
use Interitty\AssetsManager\Assets\AssetsLoaderInterface;
use Interitty\Exceptions\Exceptions;
use Interitty\Utils\FileSystem;
use Interitty\Utils\Validators;
use Nette\Caching\Cache;
use Nette\Utils\AssertionException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

use function assert;
use function call_user_func_array;

/**
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ManifestManager implements AssetsLoaderInterface
{
    /** All available cache key constants */
    public const string CACHE_ASSETS = 'assets';

    /** @var AssetFactoryInterface */
    protected AssetFactoryInterface $assetFactory;

    /** @var AssetsCollectionFactoryInterface */
    protected AssetsCollectionFactoryInterface $assetsCollFactory;

    /** @var Cache|null */
    protected ?Cache $cache = null;

    /** @var string */
    protected string $manifestPath;

    /** @var callable */
    protected $manifestParser;

    /**
     * Constructor
     *
     * @param AssetFactoryInterface $assetFactory
     * @param AssetsCollectionFactoryInterface $assetsCollFactory
     * @return void
     */
    public function __construct(
        AssetFactoryInterface $assetFactory,
        AssetsCollectionFactoryInterface $assetsCollFactory
    ) {
        $this->setAssetFactory($assetFactory);
        $this->setAssetsCollectionFactory($assetsCollFactory);
    }

    /**
     * Assets loader
     *
     * @return AssetsCollection
     */
    public function loadAssets(): AssetsCollection
    {
        $cache = $this->getCache();
        if ($cache instanceof Cache) {
            /** @var AssetsCollection $assetsCollection */
            $assetsCollection = $cache->load(
                self::CACHE_ASSETS,
                function (array|null &$dependencies): AssetsCollection {
                    /** @phpstan-var array{'files': array<string>} $dependencies */
                    $dependencies[Cache::Files][] = $this->getManifestPath();
                    return $this->parseManifestAssets();
                }
            );
        } else {
            $assetsCollection = $this->parseManifestAssets();
        }
        return $assetsCollection;
    }

    /**
     * Assets from manifest parser
     *
     * @return AssetsCollection
     */
    protected function parseManifestAssets(): AssetsCollection
    {
        $manifestContent = $this->parseManifestContent();
        $assetsCollection = $this->getAssetsCollectionFactory()->create();
        foreach ($manifestContent as $assetName => $assetPath) {
            $asset = $this->getAssetFactory()->create($assetName, $assetPath);
            $assetsCollection->addAsset($asset);
        }
        return $assetsCollection;
    }

    /**
     * Manifest content getter
     *
     * @return string[]
     */
    protected function parseManifestContent()
    {
        $manifestRawContent = $this->getManifestRawContent();
        $manifestParser = $this->getManifestParser();

        /** @var string[] $manifestContent */
        $manifestContent = call_user_func_array($manifestParser, [$manifestRawContent]);
        if (Validators::is($manifestContent, 'iterable') !== true) {
            throw Exceptions::extend(AssertionException::class)
                    ->setMessage('Manifest content is not iterable');
        }
        return $manifestContent;
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Manifest parser factory
     *
     * @return callable(string): mixed
     */
    protected function createManifestParser(): callable
    {
        $manifestParser = static function (string $manifestRawContent): mixed {
            try {
                $manifestContent = Json::decode($manifestRawContent, forceArrays: true);
                return $manifestContent;
            } catch (JsonException $exception) {
                throw Exceptions::extend(AssertionException::class)
                        ->setMessage('Manifest content is not valid JSON. :message')
                        ->setCode($exception->getCode())
                        ->addData('message', $exception->getMessage())
                        ->setPrevious($exception);
            }
        };
        return $manifestParser;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * AssetsCollectionFactory getter
     *
     * @return AssetsCollectionFactoryInterface
     */
    public function getAssetsCollectionFactory(): AssetsCollectionFactoryInterface
    {
        return $this->assetsCollFactory;
    }

    /**
     * AssetsCollectionFactory setter
     *
     * @param AssetsCollectionFactoryInterface $assetsCollFactory
     * @return static Provides fluent interface
     */
    public function setAssetsCollectionFactory(AssetsCollectionFactoryInterface $assetsCollFactory): static
    {
        assert(
            Validators::check(
                isset($this->assetsCollFactory),
                'uninitialized',
                'assetsCollectionFactory before set',
            )
        );
        $this->assetsCollFactory = $assetsCollFactory;
        return $this;
    }

    /**
     * AssetFactory getter
     *
     * @return AssetFactoryInterface
     */
    public function getAssetFactory(): AssetFactoryInterface
    {
        return $this->assetFactory;
    }

    /**
     * AssetFactory setter
     *
     * @param AssetFactoryInterface $assetFactory
     * @return static Provides fluent interface
     */
    public function setAssetFactory(AssetFactoryInterface $assetFactory): static
    {
        assert(Validators::check(isset($this->assetFactory), 'uninitialized', 'assetFactory before set'));
        $this->assetFactory = $assetFactory;
        return $this;
    }

    /**
     * Cache getter
     *
     * @return Cache|null
     */
    public function getCache(): ?Cache
    {
        return $this->cache;
    }

    /**
     * Cache setter
     *
     * @param Cache $cache
     * @return static Provides fluent interface
     */
    public function setCache(Cache $cache): static
    {
        assert(Validators::check($this->cache, 'null', 'cache before set'));
        $this->cache = $cache;
        return $this;
    }

    /**
     * ManifestParser getter
     *
     * @phpstan-return callable(string): mixed
     */
    public function getManifestParser(): callable
    {
        if ($this->manifestParser === null) {
            $manifestParser = $this->createManifestParser();
            $this->setManifestParser($manifestParser);
        }
        return $this->manifestParser;
    }

    /**
     * ManifestParser setter
     *
     * @param callable(string): mixed $manifestParser
     * @return static Provides fluent interface
     */
    public function setManifestParser(callable $manifestParser): static
    {
        assert(Validators::check($this->manifestParser, 'null', 'manifestParser before set'));
        assert(Validators::check($manifestParser, 'callable', 'ManifestParser'));
        $this->manifestParser = $manifestParser;
        return $this;
    }

    /**
     * ManifestPath getter
     *
     * @return string
     */
    public function getManifestPath(): string
    {
        assert(Validators::check($this->manifestPath, 'string', 'manifestPath before get'));
        return $this->manifestPath;
    }

    /**
     * ManifestPath setter
     *
     * @param string $manifestPath
     * @return static Provides fluent interface
     */
    public function setManifestPath(string $manifestPath): static
    {
        assert(Validators::check(isset($this->manifestPath), 'uninitialized', 'manifestPath before set'));
        if (Validators::is($manifestPath, 'readable') !== true) {
            throw Exceptions::extend(AssertionException::class)
                    ->setMessage('Manifest file ":file" is not found or readable')
                    ->addData('file', $manifestPath);
        }
        $this->manifestPath = $manifestPath;
        return $this;
    }

    /**
     * Manifest raw content getter
     *
     * @return string
     */
    protected function getManifestRawContent(): string
    {
        $manifestPath = $this->getManifestPath();
        $rawManifestContent = FileSystem::read($manifestPath);
        return $rawManifestContent;
    }

    // </editor-fold>
}
