<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Nette\DI;

use Interitty\AssetsManager\Assets\AssetFactoryInterface;
use Interitty\AssetsManager\Assets\AssetsCollectionFactoryInterface;
use Interitty\AssetsManager\AssetsManager;
use Interitty\AssetsManager\Latte\AssetsExtension;
use Interitty\AssetsManager\Manifest\ManifestManager;
use Interitty\DI\CompilerExtension;
use Interitty\Utils\Validators;
use Nette\Caching\Cache;

use function assert;

class AssetsManagerExtension extends CompilerExtension
{
    /** All config parameter name constants */
    public const string CONFIG_ASSET_BASE_PATH = 'assetBasePath';
    public const string CONFIG_CACHE = 'cache';
    public const string CONFIG_MANIFEST_PATH = 'manifestPath';
    public const string CONFIG_PRODUCTION_MODE = 'productionMode';
    public const string CONFIG_WWW_DIR = 'wwwDir';

    /** All available provider name constants */
    public const string PROVIDER_ASSET_FACTORY = 'assetFactory';
    public const string PROVIDER_ASSETS_COLLECTION_FACTORY = 'assetsCollectionFactory';
    public const string PROVIDER_ASSETS_EXTENSION = 'assetsExtension';
    public const string PROVIDER_ASSETS_MANAGER = 'assetsManager';
    public const string PROVIDER_MANIFEST_MANAGER_CACHE = 'manifestCache';
    public const string PROVIDER_MANIFEST_MANAGER = 'manifestManager';

    /**
     * @var mixed[]
     */
    protected $defaults = [
        self::CONFIG_ASSET_BASE_PATH => '',
        self::CONFIG_CACHE => '%productionMode%',
        self::CONFIG_MANIFEST_PATH => '',
        self::CONFIG_PRODUCTION_MODE => '%productionMode%',
        self::CONFIG_WWW_DIR => '%wwwDir%',
    ];

    /**
     * @inheritdoc
     */
    public function beforeCompile(): void
    {
        $this->setupAssetsManager();
        $this->setupManifestManager();
        $this->setupManifestManagerCache();
        $this->setupLatteExtension();
    }

    /**
     * @inheritdoc
     */
    public function processConfig(): array
    {
        $config = parent::processConfig();
        assert(Validators::check($config[self::CONFIG_ASSET_BASE_PATH], 'string', self::CONFIG_ASSET_BASE_PATH));
        assert(Validators::check($config[self::CONFIG_CACHE], 'bool', self::CONFIG_CACHE));
        assert(Validators::check($config[self::CONFIG_MANIFEST_PATH], 'string', self::CONFIG_MANIFEST_PATH));
        assert(Validators::check($config[self::CONFIG_PRODUCTION_MODE], 'bool', self::CONFIG_PRODUCTION_MODE));
        assert(Validators::check($config[self::CONFIG_WWW_DIR], 'string', self::CONFIG_WWW_DIR));
        return $config;
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Assets manager setup helper
     *
     * @return void
     */
    protected function setupAssetsManager(): void
    {
        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix(self::PROVIDER_ASSETS_MANAGER))
                ->setFactory(AssetsManager::class)
                ->addSetup('setAssetsBasePath', [$this->getConfigField(self::CONFIG_ASSET_BASE_PATH)])
                ->addSetup('setProductionMode', [$this->getConfigField(self::CONFIG_PRODUCTION_MODE)])
                ->addSetup('setWwwDir', [$this->getConfigField(self::CONFIG_WWW_DIR)]);
    }

    /**
     * Latte extension setup helper
     *
     * @return void
     */
    protected function setupLatteExtension(): void
    {
        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix(self::PROVIDER_ASSETS_EXTENSION))
                ->setFactory(AssetsExtension::class);

        $this->getServiceDefinition($builder, 'latte.latteFactory')
                ->addSetup('addExtension', [$this->prefix('@' . self::PROVIDER_ASSETS_EXTENSION)]);
    }

    /**
     * Manifest manager setup helper
     *
     * @return void
     */
    protected function setupManifestManager(): void
    {
        $builder = $this->getContainerBuilder();
        $configManifestPath = $this->getConfigField(self::CONFIG_MANIFEST_PATH);
        if ($configManifestPath !== '') {
            $builder->addFactoryDefinition($this->prefix(self::PROVIDER_ASSET_FACTORY))
                    ->setImplement(AssetFactoryInterface::class)
                    ->setAutowired(false);

            $builder->addFactoryDefinition($this->prefix(self::PROVIDER_ASSETS_COLLECTION_FACTORY))
                    ->setImplement(AssetsCollectionFactoryInterface::class)
                    ->setAutowired(false);

            $builder->addDefinition($this->prefix(self::PROVIDER_MANIFEST_MANAGER))
                    ->setFactory(ManifestManager::class)
                    ->setArguments([
                        $this->prefix('@' . self::PROVIDER_ASSET_FACTORY),
                        $this->prefix('@' . self::PROVIDER_ASSETS_COLLECTION_FACTORY),
                    ])
                    ->addSetup('setManifestPath', [$configManifestPath])
                    ->setAutowired(false);
            $this->getServiceDefinition($builder, $this->prefix(self::PROVIDER_ASSETS_MANAGER))
                    ->addSetup('setAssetsLoader', [$this->prefix('@' . self::PROVIDER_MANIFEST_MANAGER)]);
        }
    }

    /**
     * Manifest manager cache setup helper
     *
     * @return void
     */
    protected function setupManifestManagerCache(): void
    {
        $builder = $this->getContainerBuilder();
        $isManagerRegistered = $builder->hasDefinition($this->prefix(self::PROVIDER_MANIFEST_MANAGER));
        if (($this->getConfigField(self::CONFIG_CACHE) === true) && ($isManagerRegistered === true)) {
            $builder->addDefinition($this->prefix(self::PROVIDER_MANIFEST_MANAGER_CACHE))
                    ->setFactory(Cache::class)
                    ->setArguments([
                        'namespace' => self::PROVIDER_MANIFEST_MANAGER_CACHE,
                    ])
                    ->setAutowired(false);

            $this->getServiceDefinition($builder, $this->prefix(self::PROVIDER_MANIFEST_MANAGER))
                    ->addSetup('setCache', [$this->prefix('@' . self::PROVIDER_MANIFEST_MANAGER_CACHE)]);
        }
    }

    // </editor-fold>
}
