<?php

declare(strict_types=1);

namespace Interitty\AssetsManager;

use Interitty\AssetsManager\Assets\Asset;
use Interitty\AssetsManager\Assets\AssetsCollection;
use Interitty\AssetsManager\Assets\AssetsLoaderInterface;
use Interitty\PhpUnit\BaseTestCase;
use LogicException;
use Nette\Utils\AssertionException;
use PHPUnit\Framework\MockObject\MockObject;

use function sprintf;

/**
 * @coversDefaultClass Interitty\AssetsManager\AssetsManager
 * @phpstan-import-type AssertionExceptionData from BaseTestCase
 */
class AssetsManagerTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">

    /**
     * Asset renderer test
     *
     * @return void
     * @covers ::renderAsset
     */
    public function testRenderAsset(): void
    {
        $assetName = 'assetName';
        $assetPath = 'assetPath';
        $asset = $this->createAssetMock();
        $assetManager = $this->createAssetsManagerMock(['getAsset', 'getAssetPath']);
        $assetManager->expects(self::once())
                ->method('getAsset')
                ->with(self::equalTo($assetName))
                ->willReturn($asset);
        $assetManager->expects(self::once())
                ->method('getAssetPath')
                ->with(self::equalTo($asset))
                ->willReturn($assetPath);

        $assetHtml = $assetManager->renderAsset($assetName);
        self::assertSame($assetPath, (string) $assetHtml);
    }

    /**
     * Tester of Asset getter implementation
     *
     * @return void
     * @covers ::getAsset
     */
    public function testGetAsset(): void
    {
        $assetName = 'assetName';
        $asset = $this->createAssetMock();
        $assetsColl = $this->createAssetsCollectionMock(['getAsset']);
        $assetsColl->expects(self::once())
                ->method('getAsset')
                ->with(self::equalTo($assetName))
                ->willReturn($asset);

        $assetsManager = new AssetsManager();
        $assetsManager->setAssetsCollection($assetsColl);
        self::assertSame($asset, $assetsManager->getAsset($assetName));
    }

    /**
     * Tester of AssetBasePath getter for getting default value
     *
     * @return void
     * @covers ::getAssetsBasePath
     */
    public function testGetDefaultAssetsBasePathDefault(): void
    {
        $this->processTestGetDefault(AssetsManager::class, 'assetsBasePath', '');
    }

    /**
     * Tester of AssetBasePath getter/setter implementation
     *
     * @param string $originalPath
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getAssetsBasePath
     * @covers ::setAssetsBasePath
     */
    public function testGetSetAssetBasePath(string $originalPath): void
    {
        $this->processTestGetSet(AssetsManager::class, 'assetsBasePath', $originalPath);
    }

    /**
     * Tester of AssetsBasePath setter for inserting unsupported value
     *
     * @param mixed $originalPath
     * @phpstan-param AssertionExceptionData $data
     * @return void
     * @dataProvider unsupportedStringDataProvider
     * @covers ::setAssetsBasePath
     * @group negative
     */
    public function testSetAssetsBasePathUnsupportedValue(mixed $originalPath, array $data): void
    {
        $this->processTestSetUnsupportedValue(AssetsManager::class, 'assetsBasePath', $originalPath, $data);
    }

    /**
     * Tester of AssetsBasePath getter/setter for work with trailing slash
     *
     * @return void
     * @covers ::getAssetsBasePath
     * @covers ::setAssetsBasePath
     */
    public function testGetSetAssetsBasePathWithTrailingSlash(): void
    {
        $value = 'path';
        $trailingValue = $value . '/';

        $assetsManager = new AssetsManager();
        $assetsManager->setAssetsBasePath($trailingValue);
        self::assertSame($value, $assetsManager->getAssetsBasePath());
    }

    /**
     * Tester of AssetsCollection getter/setter implementation
     *
     * @return void
     * @covers ::getAssetsCollection
     * @covers ::setAssetsCollection
     */
    public function testGetSetAssetsCollection(): void
    {
        $className = AssetsManager::class;
        $propertyName = 'assetsCollection';
        $assetsColl = $this->createAssetsCollectionMock();

        $this->processTestGetSet($className, $propertyName, $assetsColl);
        $this->processTestSetAlreadyDefined($className, $propertyName, $assetsColl);
    }

    /**
     * Tester of AssetsCollection getter/setter for missing loader
     *
     * @return void
     * @covers ::getAssetsCollection
     * @group negative
     */
    public function testGetSetAssetsCollectionMissingLoader(): void
    {
        $assetsManager = new AssetsManager();
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected');
        $this->expectExceptionData([
            'expected' => 'initialized',
            'label' => 'assetsLoader before get',
        ]);
        $assetsManager->getAssetsCollection();
    }

    /**
     * Tester of AssetsLoder getter for work with default factory
     *
     * @return void
     * @covers ::getAssetsCollection
     * @covers ::setAssetsCollection
     */
    public function testGetAssetsLoaderCreatedCollection(): void
    {
        $assetsColl = $this->createAssetsCollectionMock();
        $assetsLoader = $this->createAssetsLoaderMock();
        $assetsLoader->expects(self::once())
                ->method('loadAssets')
                ->willReturn($assetsColl);

        $assetsManager = new AssetsManager();
        $assetsManager->setAssetsLoader($assetsLoader);
        self::assertSame($assetsColl, $assetsManager->getAssetsCollection());
    }

    /**
     * Tester of AssetsLoader getter/setter implementation
     *
     * @return void
     * @covers ::getAssetsLoader
     * @covers ::setAssetsLoader
     */
    public function testGetSetAssetsLoader(): void
    {
        $className = AssetsManager::class;
        $propertyName = 'assetsLoader';
        $assetsLoader = $this->createAssetsLoaderMock();

        $this->processTestGetSet($className, $propertyName, $assetsLoader);
        $this->processTestGetUndefined($className, $propertyName);
        $this->processTestSetAlreadyDefined($className, $propertyName, $assetsLoader);
    }

    /**
     * Tester of AssetPath getter for original path
     *
     * @return void
     * @covers ::getAssetPath
     */
    public function testGetAssetOriginalPath(): void
    {
        $assetsBasePath = 'basePath';
        $assetsOriginalPath = 'originalPath';
        $relativePath = sprintf('%s/%s', $assetsBasePath, $assetsOriginalPath);
        $productionMode = false;
        $wwwDir = (string) $this->getVfsRoot();
        $this->createTempDirectory($assetsBasePath);
        $this->createTempFile('', $relativePath);
        $asset = $this->createAssetMock(['getOriginalPath']);
        $asset->expects(self::once())
                ->method('getOriginalPath')
                ->willReturn($assetsOriginalPath);

        $assetManager = new AssetsManager();
        $assetManager->setWwwDir($wwwDir);
        $assetManager->setAssetsBasePath($assetsBasePath);
        $assetManager->setProductionMode($productionMode);
        self::assertSame($relativePath, $assetManager->getAssetPath($asset));
    }

    /**
     * Tester of AssetPath getter for production path
     *
     * @return void
     * @covers ::getAssetPath
     */
    public function testGetAssetProductionPath(): void
    {
        $assetsBasePath = 'basePath';
        $assetsProductionPath = 'productionPath';
        $relativePath = sprintf('%s/%s', $assetsBasePath, $assetsProductionPath);
        $productionMode = true;
        $wwwDir = (string) $this->getVfsRoot();
        $this->createTempDirectory($assetsBasePath);
        $this->createTempFile('', $relativePath);
        $asset = $this->createAssetMock(['getProductionPath']);
        $asset->expects(self::once())
                ->method('getProductionPath')
                ->willReturn($assetsProductionPath);

        $assetManager = new AssetsManager();
        $assetManager->setWwwDir($wwwDir);
        $assetManager->setAssetsBasePath($assetsBasePath);
        $assetManager->setProductionMode($productionMode);
        self::assertSame($relativePath, $assetManager->getAssetPath($asset));
    }

    /**
     * Tester of AssetPath getter for production path
     *
     * @return void
     * @covers ::getAssetPath
     * @group negative
     */
    public function testGetNotFoundAsset(): void
    {
        $assetsBasePath = 'basePath';
        $assetsProductionPath = 'productionPath';
        $wwwDir = (string) $this->getVfsRoot();
        $notFoundPath = sprintf('%s/%s/%s', $wwwDir, $assetsBasePath, $assetsProductionPath);
        $asset = $this->createAssetMock(['getProductionPath']);
        $asset->expects(self::once())
                ->method('getProductionPath')
                ->willReturn($assetsProductionPath);

        $assetManager = new AssetsManager();
        $assetManager->setWwwDir($wwwDir);
        $assetManager->setAssetsBasePath($assetsBasePath);
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Asset ":asset" not found');
        $this->expectExceptionData(['asset' => $notFoundPath]);
        $assetManager->getAssetPath($asset);
    }

    /**
     * Tester of ProductionMode getter/checker implementation
     *
     * @return void
     * @covers ::isProductionMode
     * @covers ::setProductionMode
     */
    public function testGetCheckProductionMode(): void
    {
        $className = AssetsManager::class;
        $propertyName = 'productionMode';

        $this->processTestGetBoolDefault($className, $propertyName, true);
        $this->processTestGetSetBool($className, $propertyName, false);
        $this->processTestGetSetBool($className, $propertyName, true);
    }

    /**
     * Tester of wwwDir getter/setter implementation
     *
     * @return void
     * @covers ::getWwwDir
     * @covers ::setWwwDir
     */
    public function testGetSetWwwDir(): void
    {
        $wwwDir = '/';
        $assetManager = new AssetsManager();
        self::assertSame($assetManager, $assetManager->setWwwDir($wwwDir));
        self::assertSame($wwwDir, $assetManager->getWwwDir());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * Asset mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Asset&MockObject
     */
    protected function createAssetMock(array $methods = []): Asset&MockObject
    {
        $mock = $this->createPartialMock(Asset::class, $methods);
        return $mock;
    }

    /**
     * AssetsCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetsCollection&MockObject
     */
    protected function createAssetsCollectionMock(array $methods = []): AssetsCollection&MockObject
    {
        $mock = $this->createPartialMock(AssetsCollection::class, $methods);
        return $mock;
    }

    /**
     * AssetsLoader mock factory
     *
     * @return AssetsLoaderInterface&MockObject
     */
    protected function createAssetsLoaderMock(): AssetsLoaderInterface&MockObject
    {
        $mock = $this->getMockForAbstractClass(AssetsLoaderInterface::class);
        return $mock;
    }

    /**
     * AssetsManager mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetsManager&MockObject
     */
    protected function createAssetsManagerMock(array $methods = []): AssetsManager&MockObject
    {
        $mock = $this->createPartialMock(AssetsManager::class, $methods);
        return $mock;
    }
    // </editor-fold>
}
