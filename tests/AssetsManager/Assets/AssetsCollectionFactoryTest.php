<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

use Interitty\PhpUnit\BaseTestCase;

use function get_class;

/**
 * @coversDefaultClass Interitty\AssetsManager\Assets\AssetsCollectionFactory
 */
class AssetsCollectionFactoryTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">

    /**
     * Tester of AssetsCollectionFactory
     *
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::create
     */
    public function testAssetsCollectionFactory(): void
    {
        $assetsCollFactory = new AssetsCollectionFactory();
        $assetsColl = $assetsCollFactory->create();
        self::assertSame(AssetsCollection::class, get_class($assetsColl));
    }
    // </editor-fold>
}
