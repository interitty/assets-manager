<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\AssetsManager\Assets\Asset
 * @phpstan-import-type AssertionExceptionData from BaseTestCase
 */
class AssetTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">

    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $originalPath = 'originalPath';
        $productionPath = 'productionPath';
        $asset = new Asset($originalPath, $productionPath);
        self::assertSame($originalPath, $asset->getOriginalPath());
        self::assertSame($productionPath, $asset->getProductionPath());
    }

    /**
     * Tester of OriginalPath getter/setter implementation
     *
     * @param string $originalPath
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getOriginalPath
     * @covers ::setOriginalPath
     */
    public function testGetSetOriginalPath(string $originalPath): void
    {
        $this->processTestGetSet(Asset::class, 'originalPath', $originalPath);
    }

    /**
     * Tester of OriginalPath setter for inserting unsupported value
     *
     * @param mixed $originalPath
     * @phpstan-param AssertionExceptionData $data
     * @return void
     * @dataProvider unsupportedStringDataProvider
     * @covers ::setOriginalPath
     * @group negative
     */
    public function testSetOriginalPathUnsupportedValue(mixed $originalPath, array $data): void
    {
        $this->processTestSetUnsupportedValue(Asset::class, 'originalPath', $originalPath, $data);
    }

    /**
     * Tester of OriginalPath setter for overwriting already defined value
     *
     * @param mixed $originalPath
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::setOriginalPath
     * @group negative
     */
    public function testSetOriginalPathAlreadyDefinedValue(mixed $originalPath): void
    {
        $this->processTestSetAlreadyDefined(Asset::class, 'originalPath', $originalPath);
    }

    /**
     * Tester of ProductionPath getter/setter implementation
     *
     * @param string $productionPath
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getProductionPath
     * @covers ::setProductionPath
     */
    public function testGetSetProductionPath(string $productionPath): void
    {
        $this->processTestGetSet(Asset::class, 'productionPath', $productionPath);
    }

    /**
     * Tester of ProductionPath setter for inserting unsupported value
     *
     * @param mixed $productionPath
     * @phpstan-param AssertionExceptionData $data
     * @return void
     * @dataProvider unsupportedStringDataProvider
     * @covers ::setProductionPath
     * @group negative
     */
    public function testSetProductionPathUnsupportedValue(mixed $productionPath, array $data): void
    {
        $this->processTestSetUnsupportedValue(Asset::class, 'productionPath', $productionPath, $data);
    }

    /**
     * Tester of ProductionPath setter for overwriting already defined value
     *
     * @param mixed $productionPath
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::setProductionPath
     * @group negative
     */
    public function testSetProductionPathAlreadyDefinedValue(mixed $productionPath): void
    {
        $this->processTestSetAlreadyDefined(Asset::class, 'productionPath', $productionPath);
    }
    // </editor-fold>
}
