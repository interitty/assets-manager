<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\AssetsManager\Assets\AssetFactory
 */
class AssetFactoryTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">

    /**
     * Tester of AssetFactory
     *
     * @return void
     * @covers ::create
     */
    public function testAssetFactory(): void
    {
        $originalPath = 'originalPath';
        $productionPath = 'productionPath';
        $assetFactory = new AssetFactory();
        $asset = $assetFactory->create($originalPath, $productionPath);
        self::assertSame($originalPath, $asset->getOriginalPath());
        self::assertSame($productionPath, $asset->getProductionPath());
    }
    // </editor-fold>
}
