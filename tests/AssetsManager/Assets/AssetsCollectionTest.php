<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Assets;

use Interitty\PhpUnit\BaseTestCase;
use LogicException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\AssetsManager\Assets\AssetsCollection
 */
class AssetsCollectionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">

    /**
     * Tester of collection adder/getter/checker implementation
     *
     * @return void
     * @covers ::isAssetExists
     * @covers ::addAsset
     * @covers ::getAsset
     */
    public function testAddGetCheckAsset(): void
    {
        $assetName = 'assetName';
        $asset = $this->createAssetMock(['getOriginalPath']);
        $asset->expects(self::once())
                ->method('getOriginalPath')
                ->willReturn($assetName);
        $assetsColl = new AssetsCollection();
        self::assertNotTrue($assetsColl->isAssetExists($assetName));
        self::assertSame($assetsColl, $assetsColl->addAsset($asset));
        self::assertTrue($assetsColl->isAssetExists($assetName));
        self::assertSame($asset, $assetsColl->getAsset($assetName));
    }

    /**
     * Tester of collection getter for non registered asset
     *
     * @return void
     * @covers ::getAsset
     * @group negative
     */
    public function testGetAssetNotRegistered(): void
    {
        $assetName = 'assetName';
        $assetsColl = new AssetsCollection();
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Asset ":asset" is not registered.');
        $this->expectExceptionData(['asset' => $assetName]);
        $assetsColl->getAsset($assetName);
    }

    /**
     * Tester of collection setter for already registered asset
     *
     * @return void
     * @covers ::addAsset
     * @group negative
     */
    public function testAddAssetAlreadyRegistered(): void
    {
        $assetName = 'assetName';
        $asset = $this->createAssetMock(['getOriginalPath']);
        $asset->expects(self::once())
                ->method('getOriginalPath')
                ->willReturn($assetName);
        $secondAsset = $this->createAssetMock(['getOriginalPath']);
        $secondAsset->expects(self::once())
                ->method('getOriginalPath')
                ->willReturn($assetName);
        $assetsColl = new AssetsCollection();
        $assetsColl->addAsset($asset);

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Asset ":asset" is already registered.');
        $this->expectExceptionData(['asset' => $assetName]);
        $assetsColl->addAsset($secondAsset);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * Asset mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Asset&MockObject
     */
    protected function createAssetMock(array $methods = []): Asset&MockObject
    {
        $mock = $this->createPartialMock(Asset::class, $methods);
        return $mock;
    }
    // </editor-fold>
}
