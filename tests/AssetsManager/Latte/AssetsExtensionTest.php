<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Latte;

use Interitty\AssetsManager\AssetsManager;
use Interitty\PhpUnit\BaseIntegrationTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\AssetsManager\Latte\AssetsExtension
 */
class AssetsExtensionTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of constructor implementation
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $assetsManager = $this->createAssetsManagerMock();
        $extension = $this->createAssetsExtensionMock(['setAssetsManager']);
        $extension->expects(self::once())->method('setAssetsManager');
        $extension->__construct($assetsManager);
    }

    /**
     * Tester of AssetsManager getter/setter implementation
     *
     * @return void
     * @covers ::getAssetsManager
     * @covers ::setAssetsManager
     */
    public function testGetSetAssetsManager(): void
    {
        $assetsManager = $this->createAssetsManagerMock();
        $this->processTestGetSet(AssetsExtension::class, 'assetsManager', $assetsManager);
    }

    /**
     * Tester of Providers getter implementation
     *
     * @return void
     * @covers ::getProviders
     */
    public function testGetProviders(): void
    {
        $assetManager = $this->createAssetsManagerMock();
        $extension = $this->createAssetsExtensionMock(['getAssetsManager']);
        $extension->expects(self::once())->method('getAssetsManager')->willReturn($assetManager);
        $result = $extension->getProviders();
        self::assertArrayHasKey(AssetsExtension::LATTE_PROVIDER_ASSETS_MANAGER, $result);
        self::assertSame($assetManager, $result[AssetsExtension::LATTE_PROVIDER_ASSETS_MANAGER]);
    }

    /**
     * Tester of Tags getter implementation
     *
     * @return void
     * @covers ::getTags
     */
    public function testGetTags(): void
    {
        $extension = $this->createAssetsExtensionMock();
        $result = $extension->getTags();
        self::assertArrayHasKey(AssetsExtension::LATTE_MACRO_NAME, $result);
        self::assertIsCallable($result[AssetsExtension::LATTE_MACRO_NAME]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * AssetsExtension mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetsExtension&MockObject
     */
    protected function createAssetsExtensionMock(array $methods = []): AssetsExtension&MockObject
    {
        $mock = $this->createPartialMock(AssetsExtension::class, $methods);
        return $mock;
    }

    /**
     * AssetsManager mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetsManager&MockObject
     */
    protected function createAssetsManagerMock(array $methods = []): AssetsManager&MockObject
    {
        $mock = $this->createPartialMock(AssetsManager::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
