<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Latte;

use Interitty\PhpUnit\BaseIntegrationTestCase;
use Nette\Bridges\ApplicationLatte\LatteFactory;
use Nette\Utils\Json;

use function sprintf;

use const DIRECTORY_SEPARATOR;

/**
 * @coversDefaultClass Interitty\AssetsManager\Latte\AssetsNode
 */
class AssetsNodeTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of AssetsNode implementation
     *
     * @return void
     * @covers ::create
     * @covers ::print
     * @covers ::getIterator
     */
    public function testAssetsNode(): void
    {
        $this->createTempDirectory($wwwDir = 'www');
        $this->createTempDirectory($wwwDir . ($assetBase = DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR));
        $sourceCss = 'css/asset.css';
        $this->createTempFile('', $wwwDir . $assetBase . ($generatedCss = 'asset.aHR0cHM6Ly9nb28uZ2wvS1ZRNHpT.css'));

        $templateContent = sprintf('{asset \'%s\'}', $sourceCss);
        $templateFile = $this->createTempFile($templateContent, 'default.latte');

        $manifestContent = Json::encode([$sourceCss => $generatedCss]);
        $manifestFile = $this->createTempFile($manifestContent, 'manifest.json');

        $configFile = $this->createTempFile(sprintf('
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    assetBasePath: %s
    manifestPath: %s
    wwwDir: %s
', $assetBase, $manifestFile, $this->getVfsRoot() . DIRECTORY_SEPARATOR . $wwwDir), 'config.neon');

        $container = $this->createContainer($configFile);
        $latteEngine = $container->getByType(LatteFactory::class)->create();
        $result = $latteEngine->renderToString($templateFile);
        self::assertSame($assetBase . $generatedCss, $result);
    }

    // </editor-fold>
}
