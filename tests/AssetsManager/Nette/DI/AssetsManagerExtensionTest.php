<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Nette\DI;

use Generator;
use Interitty\AssetsManager\AssetsManager;
use Interitty\AssetsManager\Latte\AssetsExtension;
use Interitty\AssetsManager\Manifest\ManifestManager;
use Interitty\PhpUnit\BaseIntegrationTestCase;
use Interitty\PhpUnit\BaseTestCase;
use Nette\Application\UI\TemplateFactory as TemplateFactoryInterface;
use Nette\Bridges\ApplicationLatte\TemplateFactory;
use Nette\Caching\Cache;
use Nette\Utils\AssertionException;
use PHPUnit\Framework\MockObject\MockObject;

use function assert;
use function sprintf;

/**
 * @coversDefaultClass Interitty\AssetsManager\Nette\DI\AssetsManagerExtension
 * @phpstan-import-type AssertionExceptionData from BaseTestCase
 */
class AssetsManagerExtensionTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of Assets manager setup
     *
     * @return void
     * @covers ::processConfig
     * @covers ::setupAssetsManager
     * @group integration
     */
    public function testSetupAssetsManager(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    assetBasePath: /assets
    productionMode: false
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        /* @var $assetManager AssetsManager */
        $assetManager = $container->getByType(AssetsManager::class);
        self::assertSame('/assets', $assetManager->getAssetsBasePath());
        self::assertNotTrue($assetManager->isProductionMode());
    }

    /**
     * Tester of Assets manager setup for expanded parameters
     *
     * @return void
     * @covers ::processConfig
     * @covers ::setupAssetsManager
     * @group integration
     */
    public function testSetupAssetsManagerExpanded(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    assetBasePath: /assets
    productionMode: %productionMode%
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $configurator = $this->createConfigurator($configFile);
        $configurator->setDebugMode(true);
        $container = $configurator->createContainer();

        /* @var $assetManager AssetsManager */
        $assetManager = $container->getByType(AssetsManager::class);
        self::assertSame('/assets', $assetManager->getAssetsBasePath());
        self::assertNotTrue($assetManager->isProductionMode());
    }

    /**
     * Tester of Assets manager setup for parameters expanded parameters
     *
     * @return void
     * @covers ::processConfig
     * @covers ::setupAssetsManager
     * @group integration
     */
    public function testSetupAssetsManagerParametersExpanded(): void
    {
        $configContent = '
parameters:
    isInProduction: false

extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    assetBasePath: /assets
    productionMode: %isInProduction%
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        /* @var $assetManager AssetsManager */
        $assetManager = $container->getByType(AssetsManager::class);
        self::assertSame('/assets', $assetManager->getAssetsBasePath());
        self::assertNotTrue($assetManager->isProductionMode());
    }

    /**
     * Configuration loader for work with unsupported value test data provider
     *
     * @phpstan-return Generator<string, array{0: string, 1: AssertionExceptionData}>
     */
    public function processConfigUnsupportedDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="assetBasePath">
        yield 'assetBasePath' => [
            'assetBasePath: false', ['expected' => 'string', 'label' => 'assetBasePath', 'type' => 'bool false'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="cache">
        yield 'cache' => [
            'cache: test', ['expected' => 'bool', 'label' => 'cache', 'type' => 'string \'test\''],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="manifestPath">
        yield 'manifestPath' => [
            'manifestPath: false', ['expected' => 'string', 'label' => 'manifestPath', 'type' => 'bool false'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="productionMode">
        yield 'productionMode' => [
            'productionMode: test', ['expected' => 'bool', 'label' => 'productionMode', 'type' => 'string \'test\''],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="wwwDir">
        yield 'wwwDir' => [
            'wwwDir: false', ['expected' => 'string', 'label' => 'wwwDir', 'type' => 'bool false'],
        ];
        // </editor-fold>
    }

    /**
     * Tester of Configuration loader for work with unsupported value
     *
     * @param string $neon
     * @phpstan-param AssertionExceptionData $data
     * @return void
     * @dataProvider processConfigUnsupportedDataProvider
     * @covers ::processConfig
     * @covers ::setupAssetsManager
     * @group integration
     * @group negative
     */
    public function testProcessConfigUnsupported(string $neon, array $data): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    %s
';
        $configFile = $this->createTempFile(sprintf($configContent, $neon), 'config.neon');
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected, :type given');
        $this->expectExceptionData($data);
        $this->createContainer($configFile);
    }

    /**
     * Tester of Latte extension setup
     *
     * @return void
     * @covers ::setupLatteExtension
     * @group integration
     */
    public function testSetupLatteExtension(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    manifestPath: %s
';
        $manifestFile = $this->createTempFile('', 'manifest.json');
        $configFile = $this->createTempFile(sprintf($configContent, $manifestFile), 'config.neon');
        $container = $this->createContainer($configFile);

        $templateFactory = $container->getByType(TemplateFactoryInterface::class);
        assert($templateFactory instanceof TemplateFactory);
        $assetsExtension = $container->getByType(AssetsExtension::class);
        $template = $templateFactory->createTemplate();
        $extensions = $template->getLatte()->getExtensions();
        self::assertContains($assetsExtension, $extensions);
    }

    /**
     * Tester of Manifest Manager cache setup
     *
     * @return void
     * @covers ::setupManifestManagerCache
     * @group integration
     */
    public function testSetupManifestManagerCache(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    manifestPath: %s
    cache: true
';
        $manifestFile = $this->createTempFile('', 'manifest.json');
        $configFile = $this->createTempFile(sprintf($configContent, $manifestFile), 'config.neon');

        $container = $this->createContainer($configFile);
        /* @var $assetsManager AssetsManager */
        $assetsManager = $container->getByType(AssetsManager::class);
        $manifestManager = $assetsManager->getAssetsLoader();
        assert($manifestManager instanceof ManifestManager);
        $serviceName = 'assetsManager.' . AssetsManagerExtension::PROVIDER_MANIFEST_MANAGER_CACHE;
        $cache = $container->getService($serviceName);
        self::assertInstanceOf(Cache::class, $cache);
        self::assertSame($cache, $manifestManager->getCache());
        $expectedNamespace = AssetsManagerExtension::PROVIDER_MANIFEST_MANAGER_CACHE . Cache::NAMESPACE_SEPARATOR;
        self::assertSame($expectedNamespace, $this->getNonPublicPropertyValue($cache, 'namespace'));
    }

    /**
     * Tester of Manifest Manager cache disabled setup
     *
     * @return void
     * @covers ::setupManifestManagerCache
     * @group integration
     */
    public function testSetupManifestManagerCacheDisabled(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    manifestPath: %s
    cache: false
';
        $manifestFile = $this->createTempFile('', 'manifest.json');
        $configFile = $this->createTempFile(sprintf($configContent, $manifestFile), 'config.neon');

        $container = $this->createContainer($configFile);
        $serviceName = 'assetsManager.' . AssetsManagerExtension::PROVIDER_MANIFEST_MANAGER_CACHE;
        self::assertFalse($container->hasService($serviceName));
    }

    /**
     * Tester of Manifest Manager cache no path setup
     *
     * @return void
     * @covers ::setupManifestManagerCache
     * @group integration
     */
    public function testSetupManifestManagerCacheNoPath(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    cache: true
';
        $configFile = $this->createTempFile($configContent, 'config.neon');

        $container = $this->createContainer($configFile);
        $serviceName = 'assetsManager.' . AssetsManagerExtension::PROVIDER_MANIFEST_MANAGER_CACHE;
        self::assertFalse($container->hasService($serviceName));
    }

    /**
     * Tester of Manifest Manager setup to work with manifest path
     *
     * @return void
     * @covers ::setupManifestManager
     * @group integration
     */
    public function testSetupManifestManagerWithManifestPath(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    manifestPath: %s
';
        $manifestFile = $this->createTempFile('', 'manifest.json');
        $configFile = $this->createTempFile(sprintf($configContent, $manifestFile), 'config.neon');
        $container = $this->createContainer($configFile);

        /* @var $assetManager AssetsManager */
        $assetManager = $container->getByType(AssetsManager::class);
        $manifestManager = $assetManager->getAssetsLoader();
        assert($manifestManager instanceof ManifestManager);
        self::assertSame($manifestFile, $manifestManager->getManifestPath());
    }

    /**
     * Tester of Manifest Manager setup to work with manifest path
     * Additional tests of service definition
     *
     * @return void
     * @covers ::setupManifestManager
     * @group integration
     */
    public function testSetupManifestManagerWithManifestPathServiceDefinition(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension

assetsManager:
    manifestPath: %s
';
        $manifestFile = $this->createTempFile('', 'manifest.json');
        $configFile = $this->createTempFile(sprintf($configContent, $manifestFile), 'config.neon');

        $container = $this->createContainer($configFile);
        $services = [
            'assetsManager.' . AssetsManagerExtension::PROVIDER_ASSET_FACTORY,
            'assetsManager.' . AssetsManagerExtension::PROVIDER_ASSETS_COLLECTION_FACTORY,
            'assetsManager.' . AssetsManagerExtension::PROVIDER_MANIFEST_MANAGER,
        ];
        foreach ($services as $serviceName) {
            self::assertTrue($container->hasService($serviceName));
        }
    }

    /**
     * Tester of Manifest Manager setup to work without manifest path
     *
     * @return void
     * @covers ::setupManifestManager
     * @group integration
     */
    public function testSetupManifestManagerWithoutManifestPath(): void
    {
        $configContent = '
extensions:
    assetsManager: Interitty\AssetsManager\Nette\DI\AssetsManagerExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        /* @var $assetManager AssetsManager */
        $assetManager = $container->getByType(AssetsManager::class);
        $services = [
            'assetsManager.' . AssetsManagerExtension::PROVIDER_ASSET_FACTORY,
            'assetsManager.' . AssetsManagerExtension::PROVIDER_ASSETS_COLLECTION_FACTORY,
            'assetsManager.' . AssetsManagerExtension::PROVIDER_MANIFEST_MANAGER,
        ];
        foreach ($services as $serviceName) {
            self::assertNotTrue($container->hasService($serviceName));
        }

        // Test assetsLoader is not initialized
        $this->expectException(AssertionException::class);
        $assetManager->getAssetsLoader();
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of beforeCompile
     *
     * @return void
     * @covers ::beforeCompile
     */
    public function testBeforeCompile(): void
    {
        $extension = $this->createAssetsManagerExtensionMock([
            'setupAssetsManager',
            'setupManifestManager',
            'setupManifestManagerCache',
            'setupLatteExtension',
        ]);
        $extension->expects(self::once())
            ->method('setupAssetsManager');
        $extension->expects(self::once())
            ->method('setupManifestManager');
        $extension->expects(self::once())
            ->method('setupManifestManagerCache');
        $extension->expects(self::once())
            ->method('setupLatteExtension');
        $extension->beforeCompile();
    }

    /**
     * Tester of defaults getter implementation
     *
     * @return void
     * @covers ::getDefaults
     */
    public function testGetDefaults(): void
    {
        $extension = $this->createAssetsManagerExtensionMock();
        $defaults = $this->callNonPublicMethod($extension, 'getDefaults');
        self::assertArrayHasKey(AssetsManagerExtension::CONFIG_ASSET_BASE_PATH, $defaults);
        self::assertArrayHasKey(AssetsManagerExtension::CONFIG_CACHE, $defaults);
        self::assertArrayHasKey(AssetsManagerExtension::CONFIG_MANIFEST_PATH, $defaults);
        self::assertArrayHasKey(AssetsManagerExtension::CONFIG_PRODUCTION_MODE, $defaults);
        self::assertArrayHasKey(AssetsManagerExtension::CONFIG_WWW_DIR, $defaults);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * AssetsManagerExtension mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetsManagerExtension&MockObject
     */
    protected function createAssetsManagerExtensionMock(array $methods = []): AssetsManagerExtension&MockObject
    {
        $mock = $this->createPartialMock(AssetsManagerExtension::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
