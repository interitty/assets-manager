<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Manifest;

use Interitty\AssetsManager\Assets\AssetsCollection;
use Interitty\PhpUnit\BaseTestCase;
use Nette\Caching\Cache;
use Nette\Caching\Storages\MemoryStorage;
use Nette\Utils\AssertionException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\AssetsManager\Manifest\ManifestManager
 */
class ManifestManagerCacheTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of assets loader new cache
     *
     * @return void
     * @group integration
     * @covers ::loadAssets
     */
    public function testLoadAssetsNewCache(): void
    {
        $manifestPath = 'manifestPath';
        $assetsColl = $this->createAssetsCollectionMock();
        $cacheStorage = $this->createCacheStorageMock(['write']);
        $cache = $this->createCacheMock();
        $cache->__construct($cacheStorage); // Unfortunatelly `getStorage` is not used inside original class
        $cacheKey = $this->callNonPublicMethod($cache, 'generateKey', [ManifestManager::CACHE_ASSETS]);
        $dependencies = [[Cache::Files => [$manifestPath]]];
        $cacheDependencies = $this->callNonPublicMethod($cache, 'completeDependencies', $dependencies);
        $cacheStorage->expects(self::once())
            ->method('write')
            ->with(self::equalTo($cacheKey), self::equalTo($assetsColl), self::equalTo($cacheDependencies));
        $manifestManager = $this->createManifestManagerMock(['getCache', 'getManifestPath', 'parseManifestAssets']);
        $manifestManager->expects(self::once())->method('getCache')->willReturn($cache);
        $manifestManager->expects(self::once())->method('getManifestPath')->willReturn($manifestPath);
        $manifestManager->expects(self::once())->method('parseManifestAssets')->willReturn($assetsColl);
        self::assertSame($assetsColl, $manifestManager->loadAssets());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of assets loader cached
     *
     * @return void
     * @covers ::loadAssets
     */
    public function testLoadAssetsCached(): void
    {
        $assetsColl = $this->createAssetsCollectionMock();
        $cache = $this->createCacheMock(['load']);
        $cache->expects(self::once())
            ->method('load')
            ->willReturn($assetsColl);
        $manifestManager = $this->createManifestManagerMock(['getCache', 'parseManifestAssets']);
        $manifestManager->expects(self::once())
            ->method('getCache')
            ->willReturn($cache);
        $manifestManager->expects(self::never())
            ->method('parseManifestAssets');
        self::assertSame($assetsColl, $manifestManager->loadAssets());
    }

    /**
     * Tester of Cache getter/setter implementation
     *
     * @return void
     * @covers ::getCache
     * @covers ::setCache
     */
    public function testGetSetCache(): void
    {
        $cache = $this->createCacheMock();
        $this->processTestGetSet(ManifestManager::class, 'cache', $cache);
    }

    /**
     * Tester of Cache setter for overwriting already defined value
     *
     * @return void
     * @covers ::setCache
     * @group negative
     */
    public function testSetCacheAlreadyDefinedValue(): void
    {
        $cache = $this->createCacheMock();
        $manifestManager = $this->createManifestManagerMock();
        $manifestManager->setCache($cache);
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected');
        $this->expectExceptionData([
            'expected' => 'null',
            'label' => 'cache before set',
            'type' => 'object ' . $cache::class,
        ]);
        $manifestManager->setCache($cache);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * AssetsCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetsCollection&MockObject
     */
    protected function createAssetsCollectionMock(array $methods = []): AssetsCollection&MockObject
    {
        $mock = $this->createPartialMock(AssetsCollection::class, $methods);
        return $mock;
    }

    /**
     * Cache mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Cache&MockObject
     */
    protected function createCacheMock(array $methods = []): Cache&MockObject
    {
        $mock = $this->createPartialMock(Cache::class, $methods);
        return $mock;
    }

    /**
     * Cache storage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return MemoryStorage&MockObject
     */
    protected function createCacheStorageMock(array $methods = []): MemoryStorage&MockObject
    {
        $mock = $this->createPartialMock(MemoryStorage::class, $methods);
        return $mock;
    }

    /**
     * ManifestManager mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ManifestManager&MockObject
     */
    protected function createManifestManagerMock(array $methods = []): ManifestManager&MockObject
    {
        $mock = $this->createPartialMock(ManifestManager::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
