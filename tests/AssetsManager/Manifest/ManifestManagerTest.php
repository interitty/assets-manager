<?php

declare(strict_types=1);

namespace Interitty\AssetsManager\Manifest;

use Generator;
use Interitty\AssetsManager\Assets\Asset;
use Interitty\AssetsManager\Assets\AssetFactory;
use Interitty\AssetsManager\Assets\AssetsCollection;
use Interitty\AssetsManager\Assets\AssetsCollectionFactory;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Utils\FileSystem;
use Nette\Utils\AssertionException;
use Nette\Utils\Json;
use PHPUnit\Framework\MockObject\MockObject;

use function call_user_func_array;
use function chmod;

/**
 * @coversDefaultClass Interitty\AssetsManager\Manifest\ManifestManager
 */
class ManifestManagerTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of loading JSON assets
     *
     * @return void
     * @group integration
     * @covers ::loadAssets
     * @covers ::parseManifestAssets
     */
    public function testLoadingAssets(): void
    {
        $content = Json::encode(['assetName' => 'assetpath']);
        $filePath = $this->createTempFile($content);
        $assetFactory = $this->createAssetFactoryMock();
        $assetsCollFactory = $this->createAssetsCollectionFactoryMock();
        $manifestManager = new ManifestManager($assetFactory, $assetsCollFactory);
        $manifestManager->setManifestPath($filePath);
        $assetColl = $manifestManager->loadAssets();
        $asset = $assetColl->getAsset('assetName');
        self::assertSame('assetpath', $asset->getProductionPath());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $assetFactory = $this->createAssetFactoryMock();
        $assetsCollFactory = $this->createAssetsCollectionFactoryMock();
        $manifestManager = new ManifestManager($assetFactory, $assetsCollFactory);
        self::assertSame($assetFactory, $manifestManager->getAssetFactory());
        self::assertSame($assetsCollFactory, $manifestManager->getAssetsCollectionFactory());
    }

    /**
     * Tester of assets loader
     *
     * @return void
     * @covers ::loadAssets
     */
    public function testLoadAssets(): void
    {
        $assetsColl = $this->createAssetsCollectionMock();
        $manifestManager = $this->createManifestManagerMock(['getCache', 'parseManifestAssets']);
        $manifestManager->expects(self::once())
            ->method('getCache')
            ->willReturn(null);
        $manifestManager->expects(self::once())
            ->method('parseManifestAssets')
            ->willReturn($assetsColl);
        self::assertSame($assetsColl, $manifestManager->loadAssets());
    }

    /**
     * Tester of ManifestContent parser
     *
     * @return void
     * @covers ::parseManifestContent
     */
    public function testParseManifestContent(): void
    {
        $manifestRawContent = 'manifestRawContent';
        $manifestContent = ['manifestContent'];
        $parser = static function ($rawContent) use ($manifestRawContent, $manifestContent): array {
            self::assertSame($manifestRawContent, $rawContent);
            return $manifestContent;
        };
        $manifestManager = $this->createManifestManagerMock(['getManifestRawContent', 'getManifestParser']);
        $manifestManager->expects(self::once())
            ->method('getManifestRawContent')
            ->willReturn($manifestRawContent);
        $manifestManager->expects(self::once())
            ->method('getManifestParser')
            ->willReturn($parser);

        self::assertSame($manifestContent, $this->callNonPublicMethod($manifestManager, 'parseManifestContent'));
    }

    /**
     * Tester of ManifestContent parser for non array output
     *
     * @return void
     * @covers ::parseManifestContent
     * @group negative
     */
    public function testParseManifestContentNonArray(): void
    {
        $content = 'content';
        $parser = static function (string $content): string {
            return $content;
        };
        $manifestManager = $this->createManifestManagerMock(['getManifestRawContent', 'getManifestParser']);
        $manifestManager->expects(self::once())
            ->method('getManifestRawContent')
            ->willReturn($content);
        $manifestManager->expects(self::once())
            ->method('getManifestParser')
            ->willReturn($parser);

        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('Manifest content is not iterable');
        $this->callNonPublicMethod($manifestManager, 'parseManifestContent');
    }

    /**
     * Tester of ManifestContent parser for non json input
     *
     * @return void
     * @covers ::createManifestParser
     * @group negative
     */
    public function testParseManifestContentNonJson(): void
    {
        $content = '{ \'nonJson';
        $manifestManager = $this->createManifestManagerMock(['getManifestRawContent']);
        $manifestManager->expects(self::once())
            ->method('getManifestRawContent')
            ->willReturn($content);

        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('Manifest content is not valid JSON. :message');
        $this->expectExceptionData(['message' => 'Syntax error']);
        $this->callNonPublicMethod($manifestManager, 'parseManifestContent');
    }

    /**
     * Manifest content data provider
     *
     * @phpstan-return Generator<string, array{array<string>|object, array<string>}>
     */
    public function manifestContentProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="Empty manifest array">
        yield 'Empty manifest array' => [[], []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Empty manifest object">
        yield 'Empty manifest object' => [(object) [], []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Filled manifest data">
        yield 'Filled manifest data' => [['test' => 'test'], ['test' => 'test']];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Filled manifest data as object">
        yield 'Filled manifest data as object' => [(object) ['test' => 'test'], ['test' => 'test']];
        // </editor-fold>
    }

    /**
     * Tester of the default json manifest parser factory
     *
     * @phpstan-param array<string>|object $value
     * @phpstan-param array<string> $expected
     * @return void
     * @covers ::createManifestParser
     * @dataProvider manifestContentProvider
     */
    public function testCreateManifestParser(array|object $value, array $expected): void
    {
        $json = Json::encode($value);
        $manifestManager = $this->createManifestManagerMock();
        $manifestParser = $this->callNonPublicMethod($manifestManager, 'createManifestParser');
        self::assertEquals($expected, call_user_func_array($manifestParser, [$json]));
    }

    /**
     * Tester of AssetsCollectionFactory getter/setter implementation
     *
     * @return void
     * @covers ::getAssetsCollectionFactory
     * @covers ::setAssetsCollectionFactory
     */
    public function testGetSetAssetsCollectionFactory(): void
    {
        $assetsCollFactory = $this->createAssetsCollectionFactoryMock();
        $this->processTestGetSet(ManifestManager::class, 'assetsCollectionFactory', $assetsCollFactory);
    }

    /**
     * Tester of AssetsCollectionFactory setter for overwriting already defined value
     *
     * @return void
     * @covers ::setAssetsCollectionFactory
     * @group negative
     */
    public function testSetAssetsCollectionFactoryAlreadyDefinedValue(): void
    {
        $propertyName = 'assetsCollectionFactory';
        $assetsCollFactory = $this->createAssetsCollectionFactoryMock();
        $this->processTestSetAlreadyDefined(ManifestManager::class, $propertyName, $assetsCollFactory);
    }

    /**
     * Tester of AssetFactory getter/setter implementation
     *
     * @return void
     * @covers ::getAssetFactory
     * @covers ::setAssetFactory
     */
    public function testGetSetAssetFactory(): void
    {
        $assetFactory = $this->createAssetFactoryMock();
        $this->processTestGetSet(ManifestManager::class, 'assetFactory', $assetFactory);
    }

    /**
     * Tester of AssetFactory setter for overwriting already defined value
     *
     * @return void
     * @covers ::setAssetFactory
     * @group negative
     */
    public function testSetAssetsFactoryAlreadyDefinedValue(): void
    {
        $propertyName = 'assetFactory';
        $assetFactory = $this->createAssetFactoryMock();
        $this->processTestSetAlreadyDefined(ManifestManager::class, $propertyName, $assetFactory);
    }

    /**
     * Tester of ManifestParser getter/setter implementation
     *
     * @return void
     * @covers ::getManifestParser
     * @covers ::setManifestParser
     */
    public function testGetSetManifestParser(): void
    {
        $parser = static function (string $content): mixed {
            return $content;
        };
        $manifestManager = $this->createManifestManagerMock();
        self::assertSame($manifestManager, $manifestManager->setManifestParser($parser));
        self::assertSame($parser, $manifestManager->getManifestParser());
    }

    /**
     * Tester of ManifestParser getter for getting default parser
     *
     * @return void
     * @covers ::getManifestParser
     * @depends testGetSetManifestParser
     */
    public function testGetManifestDefaultParser(): void
    {
        $parser = static function (string $content): mixed {
            return $content;
        };
        $manifestManager = $this->createManifestManagerMock(['createManifestParser']);
        $manifestManager->expects(self::once())
            ->method('createManifestParser')
            ->willReturn($parser);
        self::assertSame($parser, $manifestManager->getManifestParser());
    }

    /**
     * Tester of ManifestPath getter/setter implementation
     *
     * @return void
     * @covers ::getManifestPath
     * @covers ::setManifestPath
     */
    public function testGetSetManifestPath(): void
    {
        $filePath = $this->createTempFile();
        $manifestManager = $this->createManifestManagerMock();
        self::assertSame($manifestManager, $manifestManager->setManifestPath($filePath));
        self::assertSame($filePath, $manifestManager->getManifestPath());
    }

    /**
     * Tester of ManifestPath setter for overwriting already defined value
     *
     * @return void
     * @covers ::setManifestPath
     * @group negative
     */
    public function testSetManifestPathAlreadyDefinedValue(): void
    {
        $className = ManifestManager::class;
        $propertyName = 'manifestPath';
        $value = $this->createTempFile();
        $this->processTestSetAlreadyDefined($className, $propertyName, $value);
    }

    /**
     * Tester of ManifestPath setter for setting unreadable file
     *
     * @return void
     * @covers ::setManifestPath
     * @group negative
     */
    public function testSetManifestPathUnreadableFile(): void
    {
        $filePath = $this->createTempFile();
        chmod($filePath, 0000); // Make file unreadable
        $manifestManager = $this->createManifestManagerMock();
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('Manifest file ":file" is not found or readable');
        $this->expectExceptionData(['file' => $filePath]);
        $manifestManager->setManifestPath($filePath);
    }

    /**
     * Tester of ManifestPath setter for setting non exists file
     *
     * @return void
     * @covers ::setManifestPath
     * @group negative
     */
    public function testSetManifestPathNonExistsFile(): void
    {
        $filePath = $this->createTempFile(); // Prepare unique filename
        FileSystem::delete($filePath); // Delete that file, so it does not exists
        $manifestManager = $this->createManifestManagerMock();
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('Manifest file ":file" is not found or readable');
        $this->expectExceptionData(['file' => $filePath]);
        $manifestManager->setManifestPath($filePath);
    }

    /**
     * Tester of ManifestRawContent getter implementation
     *
     * @return void
     * @covers ::getManifestRawContent
     * @depends testGetSetManifestPath
     */
    public function testGetManifestRawContent(): void
    {
        $content = 'some testing content';
        $filePath = $this->createTempFile($content);
        $manifestManager = $this->createManifestManagerMock();
        $manifestManager->setManifestPath($filePath);
        self::assertSame($content, $this->callNonPublicMethod($manifestManager, 'getManifestRawContent'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Asset mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Asset&MockObject
     */
    protected function createAssetMock(array $methods = []): Asset&MockObject
    {
        $mock = $this->createPartialMock(Asset::class, $methods);
        return $mock;
    }

    /**
     * AssetFactory mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetFactory&MockObject
     */
    protected function createAssetFactoryMock(array $methods = []): AssetFactory&MockObject
    {
        $mock = $this->createPartialMock(AssetFactory::class, $methods);
        return $mock;
    }

    /**
     * AssetsCollectionFactory mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetsCollectionFactory&MockObject
     */
    protected function createAssetsCollectionFactoryMock(array $methods = []): AssetsCollectionFactory&MockObject
    {
        $mock = $this->createPartialMock(AssetsCollectionFactory::class, $methods);
        return $mock;
    }

    /**
     * AssetsCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssetsCollection&MockObject
     */
    protected function createAssetsCollectionMock(array $methods = []): AssetsCollection&MockObject
    {
        $mock = $this->createPartialMock(AssetsCollection::class, $methods);
        return $mock;
    }

    /**
     * ManifestManager mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ManifestManager&MockObject
     */
    protected function createManifestManagerMock(array $methods = []): ManifestManager&MockObject
    {
        $mock = $this->createPartialMock(ManifestManager::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
